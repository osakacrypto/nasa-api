# svelte-test

## @OsakaCrypto

This project showcases the following features:

- A search bar for the user to submit a query
- A results grid that displays matching images in a grid layout
- Basic pagination through a load more button

- A way to display image metadata (e.g. the image name and location)
- Filters for the image grid to display solar system planets queries
- A zoom control to change the size of images in the grid
- A way for the user to select multiple images in the grid to download (still to do)

This project makes use of the NASA public API. <https://api.nasa.gov/>

This is a project built using the following stack:
Svelte,
SvelteKit,
SvelteCube
Typescript,
Three.js,
Gitlab,
Hostinger,
API
## Running this project 🏃

cd nasaAPI

npm install

npm run dev

## Authors and acknowledgment

@OsakaCrypto.
Shout out to the Svelte community for all the open source libraries / packages and contributions.

## License

This is open source. Please feel free to use, fork and contribute!

## Project status

Working draft. Next steps are to add a download capability and also clean up the css to use vars(). 
The final nice to have is an interface for the api so that all the response items are typed.
