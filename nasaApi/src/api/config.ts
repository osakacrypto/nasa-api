// USEFUL ENUMS
export const API_BASE_URL = "https://images-api.nasa.gov/search?q=";

export const MERCURY_PATH = "mercury";
export const VENUS_PATH = "venus";
export const EARTH_PATH = "earth";
export const MARS_PATH = "mars";
export const JUPITER_PATH = "jupiter";
export const SATURN_PATH = "saturn";
export const URANUS_PATH = "uranus";
export const NEPTUNE_PATH = "neptune";
export const PLUTO_PATH = "pluto";
export const SUN_PATH = "sun";
export const IMAGE_ONLY = "&media_type=image";