import * as THREE from 'three';

import GLTFLoader from 'three-gltf-loader';

import issModel from '$lib/threejs/space-station.glb';

const isBrowser = typeof window !== 'undefined';

const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(75, isBrowser ? window.innerWidth / window.innerHeight : 1, 0.1, 1000);

let renderer;
if (isBrowser) {
  renderer = new THREE.WebGLRenderer({ antialias: true });
  document.body.appendChild(renderer.domElement);
}

const modelLoader = new GLTFLoader();
let spaceStation;
modelLoader.load({issModel}, (gltf) => {
  spaceStation = gltf.scene;
  scene.add(spaceStation);
  console.log('Space station loaded:', spaceStation);
}, undefined, (error) => {
  console.error(error);
});

camera.position.z = 5;

const animate = () => {
  requestAnimationFrame(animate);
  if (spaceStation) {
    spaceStation.rotation.y += 0.01;
  }
  renderer.render(scene, camera);
};

const resize = () => {
  if (isBrowser) {
    renderer.setSize(window.innerWidth, window.innerHeight)
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
  }
};

export const createScene = (el) => {
  if (isBrowser) {
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    el.appendChild(renderer.domElement);
    resize();
    animate();
  }
}

if (isBrowser) {
  window.addEventListener('resize', resize);
}
