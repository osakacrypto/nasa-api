import { writable, readable, derived } from 'svelte/store';

// Contains the status of all models
export const statusOfModels = writable({}); // { uniqueName: 'LOADING' | 'ERROR' | 'SUCCESS' }

// Returns a boolean if any model has a status of loading
export const modelsLoading = derived(statusOfModels, statusObj => {
  return Object.values(statusObj).includes('LOADING');
})

// Updates a model's status based on its unique name
export const updateModelStatus = (name, status) => {
  statusOfModels.update(current => {
    return {  
      ...current,
      [name]: status,
    }
  })
}

// List of example model URLs
export const modelURL = {
  iss: 'space-station.glb',
}

// Loads the space station model
export async function loadSpaceStation(event) {
  const response = await event.fetch('/space-station.glb');
  const buffer = await response.arrayBuffer();
  const blob = new Blob([buffer], { type: 'model/gltf-binary' });
  return blob;
}

export const spaceStationStore = writable(null);
